<?php

namespace App\Controller;

use App\Entity\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class TodoController extends AbstractController
{

    /**
     * @Route("/todo", name="index_todo")
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $todos = $this->getDoctrine()
        ->getRepository(Todo::class)
        ->findBy(array(), array('id'=>'desc'));

        $pagination = $paginator->paginate(
            $todos,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('todo/index.html.twig', [
            'controller_name' => 'TodoController',
            //'todos' => $todos,
            'todos' => $pagination
        ]);
    }

    /**
     * @Route("/todo/create", name="create_todo")
     */
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $todo = new Todo();

        $form = $this->createFormBuilder($todo)
            ->add('title', TextType::class, array(
                'attr' => ['class' => 'form-control mb-2'])
                )
            ->add('description', TextareaType::class, array(
                'attr' => ['class' => 'form-control'])
                )
            ->add('status', HiddenType::class, array(
                'data' => 0)
                )
            ->add('create', SubmitType::class, array(
                'attr' => ['class' => 'btn btn-outline-dark mt-3 float-right'])
                )
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $todo = $form->getData();
                
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($todo);
                $entityManager->flush();

                return $this->redirectToRoute('index_todo');
            }

        return $this->render('todo/create.html.twig', [
            'controller_name' => 'TodoController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/todo/edit/{id}", name="edit_todo")
     */
    public function edit(Request $request, $id)
    {
        
        $this->denyAccessUnlessGranted('ROLE_MODERATOR');

        $todo = $this->getDoctrine()
        ->getRepository(Todo::class)
        ->find($id);

        $form = $this->createFormBuilder($todo)
            ->add('title', TextType::class, array(
                'attr' => ['class' => 'form-control mb-2'])
                )
            ->add('description', TextareaType::class, array(
                'attr' => ['class' => 'form-control mb-2'])
                )
            ->add('status', ChoiceType::class, array(
                'attr' => ['class' => 'form-control'],
                'choices'  => [
                    'Done' => true,
                    'Pending' => false,
                ],)
                )
                ->add('cancel', ButtonType::class, array(
                    'attr' => [
                        'class' => 'btn btn-outline-dark mt-3 float-left']
                        )
                    )
            ->add('update', SubmitType::class, array(
                'attr' => ['class' => 'btn btn-outline-dark mt-3 float-right'])
                )
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
               
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();

                return $this->redirectToRoute('index_todo');
            }

        return $this->render('todo/edit.html.twig', [
            'controller_name' => 'TodoController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/todo/delete/{id}", name="delete_todo")
     */
    public function delete(Request $request, $id) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $todo = $this->getDoctrine()
        ->getRepository(Todo::class)
        ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($todo);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }

     /**
     * @Route("/todo/update/{id}/{status}", name="update_todo")
     */
    public function update(Request $request, $id, $status) {
        
        $todo = $this->getDoctrine()
        ->getRepository(Todo::class)
        ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $todo->setStatus($status);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }


}
