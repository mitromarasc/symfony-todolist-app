<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createFormBuilder($user)
        ->add('Email', EmailType::class, array(
            'attr' => ['class' => 'form-control mb-2'])
            )
            ->add('Password', PasswordType::class, array(
                'attr' => ['class' => 'form-control mb-2'])
                )
                ->add('Roles', ChoiceType::class, array(
                    'attr' => ['class' => 'form-control'],
                    'choices'  => [
                        'User' => null,
                        'Moderator' => 'ROLE_MODERATOR',
                        'Admin' => 'ROLE_ADMIN',
                    ])
                )
                ->add('Register', SubmitType::class, array(
                    'attr' => ['class' => 'btn btn-outline-dark mt-3 float-right'])
                    )
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('Password')->getData()
                )
            );

            $role =  $form->get('Roles')->getData();

            switch ($role) {
                case "0": $role = ''; break;
                case "1": $role = 'ROLE_MODERATOR'; break;
                case "2": $role = 'ROLE_ADMIN';
            }

            $user->setRoles($role);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $this->redirectToRoute('index_todo');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
