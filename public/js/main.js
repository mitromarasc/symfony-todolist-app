const cancel = document.getElementById("form_cancel");
if (cancel) {
    cancel.addEventListener('click', (e) => { 
        window.location = '/todo';
    });
}

const todos = document.getElementById("todos");

if (todos) {
    todos.addEventListener('click', (e) => {
        if(e.target.className === 'btn btn-outline-danger'){
            if(confirm("Are you sure you want to delete this item")){
                const id = e.target.getAttribute('data-id');
               
                fetch(`/todo/delete/${id}`, {
                    method: 'DELETE'
                }).then(res => window.location.reload());
                
            }
        }
        else if(e.target.className === 'custom-control-input'){
            
                const id = e.target.getAttribute('data-id');
                const checked = e.target.checked;

                if (checked == true) status = 1;
                else status = 0;
                
                fetch(`/todo/update/${id}/${status}`, {
                    method: 'UPDATE'
                }).then(res => window.location.reload());
            
        }
    });
}

const paginator = document.getElementsByClassName("pagination");
if (paginator) {

}